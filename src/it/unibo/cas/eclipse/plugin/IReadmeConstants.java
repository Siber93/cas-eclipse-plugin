package it.unibo.cas.eclipse.plugin;

public interface IReadmeConstants {
    String PLUGIN_ID = "it.unibo.cas.eclipse.plugin"; //$NON-NLS-1$

    String PREFIX = PLUGIN_ID + "."; //$NON-NLS-1$

    String BUGZILLA_PORT = PREFIX + "bugzilla_port"; 
    
    String GITLAB_PORT = PREFIX + "gitlab_port"; 
    
    String MATTERMOST_PORT = PREFIX + "mattermost_port"; 
    
    String SONAR_PORT = PREFIX + "sonar_port"; 
    
    String TAIGA_PORT = PREFIX + "taiga_port"; 
    
    String SERVER_URL = PREFIX + "server_url"; 
    
    String PREFERENCE_PAGE_CONTEXT = PREFIX
    		+ "preference_page_context";

}