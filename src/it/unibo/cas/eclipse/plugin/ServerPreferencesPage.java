package it.unibo.cas.eclipse.plugin;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;

/**
 * This class implements a sample preference page that is
 * added to the preference dialog based on the registration.
 */
public class ServerPreferencesPage extends PreferencePage implements
        IWorkbenchPreferencePage, SelectionListener, ModifyListener {

    private Text bugzillaTextField;
    private Text gitlabTextField;
    private Text mattermostTextField;
    private Text sonarTextField;
    private Text taigaTextField;
    private Text serverTextField;

    /**
     * Creates an new checkbox instance and sets the default
     * layout data.
     *
     * @param group  the composite in which to create the checkbox
     * @param label  the string to set into the checkbox
     * @return the new checkbox
     */
    private Button createCheckBox(Composite group, String label) {
        Button button = new Button(group, SWT.CHECK | SWT.LEFT);
        button.setText(label);
        button.addSelectionListener(this);
        GridData data = new GridData();
        button.setLayoutData(data);
        return button;
    }

    /**
     * Creates composite control and sets the default layout data.
     *
     * @param parent  the parent of the new composite
     * @param numColumns  the number of columns for the new composite
     * @return the newly-created coposite
     */
    private Composite createComposite(Composite parent, int numColumns) {
        Composite composite = new Composite(parent, SWT.NULL);

        //GridLayout
        GridLayout layout = new GridLayout();
        layout.numColumns = numColumns;
        composite.setLayout(layout);

        //GridData
        GridData data = new GridData();
        data.verticalAlignment = GridData.FILL;
        data.horizontalAlignment = GridData.FILL;
        composite.setLayoutData(data);
        return composite;
    }

    /** (non-Javadoc)
     * Method declared on PreferencePage
     */
    @Override
	protected Control createContents(Composite parent) {
    	PlatformUI.getWorkbench().getHelpSystem().setHelp(parent,
				IReadmeConstants.PREFERENCE_PAGE_CONTEXT);


        Composite composite_bugzilla = createComposite(parent, 2);
        createLabel(composite_bugzilla, "Bugzilla Port"); //$NON-NLS-1$
        bugzillaTextField = createTextField(composite_bugzilla);
        

        Composite composite_gitlab = createComposite(parent, 2);
        createLabel(composite_gitlab, "GitLab Port"); //$NON-NLS-1$
        gitlabTextField = createTextField(composite_gitlab);
        
  

        Composite composite_mattermost = createComposite(parent, 2);
        createLabel(composite_mattermost, "Mattermost Port"); //$NON-NLS-1$
        mattermostTextField = createTextField(composite_mattermost);
        
        
        Composite composite_sonar = createComposite(parent, 2);
        createLabel(composite_sonar, "Sonar Port"); //$NON-NLS-1$
        sonarTextField = createTextField(composite_sonar);
        
        
        Composite composite_taiga = createComposite(parent, 2);
        createLabel(composite_taiga, "Taiga Port"); //$NON-NLS-1$
        taigaTextField = createTextField(composite_taiga);
        
       
        Composite composite_server = createComposite(parent, 2);
        createLabel(composite_server, "Server URL"); //$NON-NLS-1$
        serverTextField = createTextField(composite_server);
        
        
        initializeValues();

        //font = null;
        return new Composite(parent, SWT.NULL);
    }

    /**
     * Utility method that creates a label instance
     * and sets the default layout data.
     *
     * @param parent  the parent for the new label
     * @param text  the text for the new label
     * @return the new label
     */
    private Label createLabel(Composite parent, String text) {
        Label label = new Label(parent, SWT.LEFT);
        label.setText(text);
        GridData data = new GridData();
        data.horizontalSpan = 2;
        data.horizontalAlignment = GridData.FILL;
        label.setLayoutData(data);
        return label;
    }

    /**
     * Utility method that creates a push button instance
     * and sets the default layout data.
     *
     * @param parent  the parent for the new button
     * @param label  the label for the new button
     * @return the newly-created button
     */
    private Button createPushButton(Composite parent, String label) {
        Button button = new Button(parent, SWT.PUSH);
        button.setText(label);
        button.addSelectionListener(this);
        GridData data = new GridData();
        data.horizontalAlignment = GridData.FILL;
        button.setLayoutData(data);
        return button;
    }

    /**
     * Utility method that creates a radio button instance
     * and sets the default layout data.
     *
     * @param parent  the parent for the new button
     * @param label  the label for the new button
     * @return the newly-created button
     */
    private Button createRadioButton(Composite parent, String label) {
        Button button = new Button(parent, SWT.RADIO | SWT.LEFT);
        button.setText(label);
        button.addSelectionListener(this);
        GridData data = new GridData();
        button.setLayoutData(data);
        return button;
    }

    /**
     * Create a text field specific for this application
     *
     * @param parent  the parent of the new text field
     * @return the new text field
     */
    private Text createTextField(Composite parent) {
        Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
        text.addModifyListener(this);
        GridData data = new GridData();
        data.horizontalAlignment = GridData.FILL;
        data.grabExcessHorizontalSpace = true;
        data.verticalAlignment = GridData.CENTER;
        data.grabExcessVerticalSpace = false;
        text.setLayoutData(data);
        return text;
    }

    /**
     * The <code>ReadmePreferencePage</code> implementation of this
     * <code>PreferencePage</code> method
     * returns preference store that belongs to the our plugin.
     * This is important because we want to store
     * our preferences separately from the workbench.
     */
    @Override
	protected IPreferenceStore doGetPreferenceStore() {
        return PluginDB.getDefault().getPreferenceStore();
    }

    @Override
	public void init(IWorkbench workbench) {
        // do nothing
    }

    /**
     * Initializes states of the controls using default values
     * in the preference store.
     */
    private void initializeDefaults() {
        IPreferenceStore store = getPreferenceStore();
        
        bugzillaTextField.setText("9000");
        
        gitlabTextField.setText("9001");
        
        mattermostTextField.setText("9002");
        
        sonarTextField.setText("9003");
        
        taigaTextField.setText("9004");
        
        serverTextField.setText("127.0.0.1");
    }

    /**
     * Initializes states of the controls from the preference store.
     */
    private void initializeValues() {
        IPreferenceStore store = getPreferenceStore();
        
        bugzillaTextField.setText(store.getString(IReadmeConstants.BUGZILLA_PORT));
        
        gitlabTextField.setText(store.getString(IReadmeConstants.GITLAB_PORT));
        
        mattermostTextField.setText(store.getString(IReadmeConstants.MATTERMOST_PORT));
        
        sonarTextField.setText(store.getString(IReadmeConstants.SONAR_PORT));
        
        taigaTextField.setText(store.getString(IReadmeConstants.TAIGA_PORT));
        
        serverTextField.setText(store.getString(IReadmeConstants.SERVER_URL));
    }

    @Override
	public void modifyText(ModifyEvent event) {
        //Do nothing on a modification in this example
    }

    @Override
	protected void performDefaults() {
        super.performDefaults();
        initializeDefaults();
    }

    @Override
	public boolean performOk() {
        storeValues();
        PluginDB.getDefault().savePluginPreferences();
        return true;
    }

    /**
     * Stores the values of the controls back to the preference store.
     */
    private void storeValues() {
    	
        IPreferenceStore store = getPreferenceStore();
        
        store.setValue(IReadmeConstants.BUGZILLA_PORT, bugzillaTextField.getText());
        
        store.setValue(IReadmeConstants.GITLAB_PORT, gitlabTextField.getText());
        
        store.setValue(IReadmeConstants.MATTERMOST_PORT, mattermostTextField.getText());
        
        store.setValue(IReadmeConstants.SONAR_PORT, sonarTextField.getText());
        
        store.setValue(IReadmeConstants.TAIGA_PORT, taigaTextField.getText());
        
        store.setValue(IReadmeConstants.SERVER_URL, serverTextField.getText());        
        
    }

    /**
     * Creates a tab of one horizontal spans.
     *
     * @param parent  the parent in which the tab should be created
     */
    private void tabForward(Composite parent) {
        Label vfiller = new Label(parent, SWT.LEFT);
        GridData gridData = new GridData();
        gridData = new GridData();
        gridData.horizontalAlignment = GridData.BEGINNING;
        gridData.grabExcessHorizontalSpace = false;
        gridData.verticalAlignment = GridData.CENTER;
        gridData.grabExcessVerticalSpace = false;
        vfiller.setLayoutData(gridData);
    }

    @Override
	public void widgetDefaultSelected(SelectionEvent event) {
        //Handle a default selection. Do nothing in this example
    }

    @Override
	public void widgetSelected(SelectionEvent event) {
        //Do nothing on selection in this example;
    }
}