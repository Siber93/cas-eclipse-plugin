package it.unibo.cas.eclipse.plugin;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.part.*;
import org.eclipse.jface.viewers.*;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.jface.action.*;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.*;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;

import javax.annotation.PostConstruct;
import javax.inject.Inject;


public class CasView extends ViewPart {

	private Browser myBrowser;

	@Override
	public void createPartControl(Composite parent) {
		

		IPreferenceStore store =  PluginDB.getDefault().getPreferenceStore();
		

		GridLayout maingrid = new GridLayout();
		maingrid.numColumns = 1;
		parent.setLayout(maingrid);
		
		
		// 1� ROW
		Label title = new Label(parent, SWT.FLAT | SWT.LEFT);
		title.setText("CAS");
		
		
		
		// 2� ROW
		
		Composite compositeButtons = new Composite(parent, SWT.FLAT);
		compositeButtons.setLayout(new RowLayout());
		compositeButtons.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		
		
		// #########################################################
		
		Listener openBugzillaURLListener = new Listener() {
		      public void handleEvent(Event event) {
		        myBrowser.setUrl(store.getString(IReadmeConstants.SERVER_URL) + ":" + store.getString(IReadmeConstants.BUGZILLA_PORT));
		      }
		    };
		
	    Button bugzillaBtn = new Button(compositeButtons, SWT.NULL);
	    bugzillaBtn.setText("Bugzilla");
	    bugzillaBtn.addListener(SWT.MouseUp, openBugzillaURLListener);
		

		// #########################################################
		
		Listener openGitlabURLListener = new Listener() {
		      public void handleEvent(Event event) {
		        myBrowser.setUrl(store.getString(IReadmeConstants.SERVER_URL) + ":" + store.getString(IReadmeConstants.GITLAB_PORT));
		      }
		    };
		
	    Button gitlabBtn = new Button(compositeButtons, SWT.NULL);
	    gitlabBtn.setText("GitLab");
	    gitlabBtn.addListener(SWT.MouseUp, openGitlabURLListener);	    
	    // #########################################################
	    
	    Listener openMattermostURLListener = new Listener() {
		      public void handleEvent(Event event) {
		        myBrowser.setUrl(store.getString(IReadmeConstants.SERVER_URL) + ":" + store.getString(IReadmeConstants.MATTERMOST_PORT));
		      }
		    };		
	    
	    Button mattermostbBtn = new Button(compositeButtons, SWT.NULL);
	    mattermostbBtn.setText("Mattermost");
	    mattermostbBtn.addListener(SWT.MouseUp, openMattermostURLListener);
	    // #########################################################	    
	    
	    Listener openSonarqubeURLListener = new Listener() {
		      public void handleEvent(Event event) {
		        myBrowser.setUrl(store.getString(IReadmeConstants.SERVER_URL) + ":" + store.getString(IReadmeConstants.SONAR_PORT));
		      }
		    };	
		    
	    
	    Button sonarqubeBtn = new Button(compositeButtons, SWT.NULL);
	    sonarqubeBtn.setText("Sonarqube");
	    sonarqubeBtn.addListener(SWT.MouseUp, openSonarqubeURLListener);
	    // #########################################################
	    
	    Listener openTaigaURLListener = new Listener() {
		      public void handleEvent(Event event) {
		        myBrowser.setUrl(store.getString(IReadmeConstants.SERVER_URL) + ":" + store.getString(IReadmeConstants.TAIGA_PORT));
		      }
		    };	
	    
	    
	    Button taigaBtn = new Button(compositeButtons, SWT.NULL);
	    taigaBtn.setText("Taiga");
	    taigaBtn.addListener(SWT.MouseUp, openTaigaURLListener);  
	    // #########################################################
	    
		// 3� ROW
		
	    Composite compositeLocation = new Composite(parent, SWT.FLAT);
	    compositeLocation.setLayout(new GridLayout(3, false));
	    compositeLocation.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

	    
	    // 4� ROW
	    myBrowser = new Browser(parent, SWT.BORDER);
	    myBrowser.setLayoutData(new GridData(GridData.FILL_BOTH));
	    myBrowser.setUrl("www.google.com");

	    
	    title.pack();
	    parent.pack();
	    

	   

	}

	@Override
	public void setFocus() {
		//serverIPLbl.setFocus();

	}

	
}
