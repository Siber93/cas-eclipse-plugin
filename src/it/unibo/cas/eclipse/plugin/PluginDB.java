package it.unibo.cas.eclipse.plugin;
import org.eclipse.ui.plugin.AbstractUIPlugin;

public class PluginDB extends AbstractUIPlugin {
	public static final String PLUGIN_ID = IReadmeConstants.PLUGIN_ID; //$NON-NLS-1$

    /**
     * Default instance of the receiver
     */
    private static PluginDB inst = new PluginDB();

    /**
     * Creates the Readme plugin and caches its default instance
     */
    public PluginDB() {
        if (inst == null)
            inst = this;
    }

    /**
     * Gets the plugin singleton.
     *
     * @return the default ReadmePlugin instance
     */
    static public PluginDB getDefault() {
        return inst;
    }

}
